# README #

This script attempts to perform an in-line upgrade of an *existing* Ureka installation.

`ur_upgrade` may be useful if you have invested time customizing your environment, and are wary of installing the latest version manually.

### Usage ###

```
usage: ur_upgrade.py [-h] [--list-available] [--latest] [--request REQUEST]
                     [--mirror MIRROR] [--no-backup] [--backup-dir] [--force]
                     UR_DIR

Ureka Upgrade Utility

positional arguments:
  UR_DIR             Absolute path to Ureka installation

optional arguments:
  -h, --help         show this help message and exit
  --list-available   List available releases
  --latest           Upgrade to the latest (stable) version
  --request REQUEST  Upgrade to a specific version
  --mirror MIRROR    Use a Ureka download mirror
  --no-backup        Do not backup existing installation
  --backup-dir       Alternative backup storage location
  --force            Ignore version checking
```

### License ###

BSD